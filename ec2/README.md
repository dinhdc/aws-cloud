# Elastic Cloud Compute (EC2)

## EC2 Instance Types

1. Naming Convension

    - **m5.2xlarge**:
        - m: instance class
        - 5: generation (AWS improves them over time)
        - 2xlarge: size within the instance class

2. General Purpose

    - Greate for some workloads as: **web servers, code repositories**
    - Balance between: *Compute*, *Networking* and *Memory*

3. Compute Optimized

    - Greate for some tasks requre **high performance processors**
        - *Batch processing workloads*
        - *Media transcoding*
        - *High performance web servers*
        - *High performance computing (HPC)*
        - *Scientific modeling & machine learning*
        - *Dedicated gaming servers*

4. Memory Optimized

    - Greate for workloads that **process large data sets in memory**
    - Use cases:
        - *High performance, relation/non-relation databases*
        - *Distributed web scale cache stores*
        - *In-memory databases optimized for BI*
        - *Application performing real-time processing for big unstructured data*

5. Storage Optimized 

    - Greate for storage-intensive tasks that require **high, sequential read and write access to large data sets on local storage**
    - Use cases:
        - *High frequency online transaction processing (OLTP) system*
        - *Relational & NoSql databases*
        - *Cache for in-memomry database (Redis)*
        - *Data warehousing application*
        - *Distributed file system*

## Security groups (SG)

1. Basic

    - SG are the fundamental of network security in AWS
    - They control **how traffic is allowed into or our of EC2 instances**.
    - SG only contan **allow** rules.
    - SG rules can reference **by IP** or **by other security group**

2. Deeper Dive

    - SG are acting as a **firewall** on EC2 instances.
    - They regulate (distribute):
        - *Access to Ports*
        - *Authroised IP ranges - IPv4 and IPv6*
        - *Control of inbound network (from other to instance)*
        - *Control of outbound network (from instance to other)*

3. Good to know

    - Can be attached to **multiple instances**
    - Locked down to a **Region / VPC** combination
    - Does live "outside" the EC2 - if traffic is blocked, the EC2 instance won't see it
    - **It's good to maintain one separate security group for SSH access**
    - If your application is not accessible (**time out**), then it's a SG issue
    - If your application gives a **"connection refused"** error, then it's an application error or it's not launched
    - All inbound traffic is **blocked** by default
    - All outbound traffic is **allowed** by default

4. Classic Ports to know

    - **22 = SSH** - log into a Linux instance
    - **21 = FTP (File Transfer Protocol)** - upload files into a file share
    - **22 = SFTP (Security File Transfer Protocol)** - upload files using SSH
    - **80 = HTTP** - access unsecured websites
    - **443 = HTTPS** - access secured websites
    - **3389 = RDP (Remote Desktop Protocol)** - log into a Windows instance

## Purchasing Options

1. On Demand

    - Pay for what you use:
        - Linux or Windows - billing per second, after the first minute
        - All other operating system - billing per hour
    - Has **the highest cost** but **no upfront payment**
    - No long-term commitment
    - Recommend for **short-term** and **un-interrupted workloads**, where you can't predict how the application will behave

2. Reserved Instances

    - Up to **72%** discount compared to On-demand
    - You reserve a specific instance attributes **(Instance Type, Region, Tenacy, OS)**
    - **Reservation Period** - **1 year**(+discount) or **3 years**(+++discount)
    - **Payment Options** - **No Upfront(+), Partial Upfront (++), All Upfront(++)**
    - **Reserved Instance's Scope** - **Regional** or **Zonal** (reserve capicity in an AZ)
    - Recommend for steady-state usage applications (think database)
    - You can buy or sell in the Reserved Instance Marketplace
    - **Convertible Reserved Instance**:
        - Can change the EC2 isntance type, instance family, OS, scope and tenacy
        - Up to **60%** discount

3. Saving Plans

    - Get a discount based on long-term usage (up to 72% - same as RIs)
    - Commit to a certain type of usage ($10/hour for 1 or 3 years)
    - Usage beyond EC2 Savings Plans is billed at the On-Demand price
    - Locked to a specific instance family & AWS region (e.g, M5 in us-east-1)
    - Flexible across:
        - Instance Size (m5.large, m5.2xlarge)
        - OS (Linux, Windows)
        - Tenancy (Host, Dedicated, Default)
    
4. Spot Instances

    - Can get a discount of up to **90%** compared to On-Demand
    - Instance that you can "lose" at any point of time if your max price is less than the current spot price
    - The **MOIST cost-efficient** instances in AWS
    - **Usefull for workloads that are resilient to failure**:
        - Batch jobs
        - Data analysis
        - Image processing
        - Any **distributed** workloads
        - Workloads with a flexible start and end time
    - Not **suitable for critical jobs or databases**
    - Requests: 
        - Define **max spot price** and get the instance while **current spot price** < **max**
            - The hourly spot price varies base on offer and capacity
            - If the current spot price > your max price, you can choose **stop** or **terminate** your instance with 2 minutes grace period
        - Other stragy: **Spot Block**
            - "block" spot instance during a specified time frame (1 - 6 hours) without interruptions 
            - In rare situations, the instance may be **reclaimed** 


5. Dedicated Hosts

    - A physical server with EC2 instance capacity fully dedicated to your use
    - Allows you address **compliance requirements** and **use your existing server-bound software licenses** (per-socket, per-core, pe-VM software licenses)
    - Purchasing Options:
        - **On-demand** - pay per second for active Dedicated Host
        - **Reserved** - 1 or 3 years (No upfront, Patial upfront, all upfront)
    - **The most expensive option**
    - Useful for:
        - software that have complicated licensing model (BYOL - Bring Your Own License)
        - company that have strong regulatory or compliance needs

6. Capacity Reservation

    - Reserve **On-Demand** instances capacity in a specific AZ for any duration
    - You always have access to EC2 capacity when you need id
    - **No time commitment** (create/cancel anytime), **no billing discounts**
    - Combine with Region Reserved Instance and Savings Plans to benefit from billing discounts
    - You're charge at On-Demand rate whether you run instances or not
    - Suitable for short-term, uninterrupted workloads that needs to be in a specific AZ

7. Spot Fleet

    - set of Spot Instances + (optional) On-Demand Instances
    - The Spot Fleet try to meet the target  capacity with price constraints
        - Define possible lauch pools: instance type (m5.large), OS, Availability Zone
        - Can have multiple lauch pools, so that the fleet can choose
        - Spot Fleet stops lauching instances when reaching capacity or max cost
    - Strategies to allocate Spot Instances
        - **lowest price**: from the pool with the lowest price (cost optimization, short workload)
        - **diversified**: distributed across all pools (great for availability, long workloads)
        - **capacity optimized**: pool with the optimized capacity for the number of instances
        - **price capacity optimized (Recommended)**: pools with highest capacity available, then select the pool with the lowest price (best choice for most workloads)
    
    - <ins>Spot Fleets allow us to automatically request Spot Instances with the lowest price</ins>